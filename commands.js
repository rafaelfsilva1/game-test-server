var _ = require('lodash');

function isCommand(message) {
  var result =  _.startsWith(message, '/');
  return result;
}

function userPrivateMessageCommand(socket, message) {
  var toUsername = _.first(_.words(message));
  return  {
    name:  toUsername + ':inbox',
    data: {
      username: socket.username,
      message: message.replace('/' + toUsername, '')
    }
  }
}

function executeCommand(socket, command) {
  var commandObject = userPrivateMessageCommand(socket, command);
  socket.broadcast.emit(commandObject.name, commandObject.data);
}

module.exports = {
  isCommand: isCommand,
  executeCommand: executeCommand
}
