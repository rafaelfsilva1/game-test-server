var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var namespaces = require('./namespaces')(io);
var events = require('./events');
var port = process.env.PORT || 3000;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});


var time_namespace = namespaces.create('time');

events.attachAll(io);
events.attachAll(time_namespace);
