module.exports = function (io) {
  var namespaces = [io];

  function count(){
    return namespaces.length;
  }

  function create(name) {
    var namespace = io.of('/' + name);
    namespaces.push(namespace);

    return namespace;
  }

  return {
    count: count,
    create: create
  }
}
